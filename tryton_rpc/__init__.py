#!/usr/bin/env python
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import http.client
import logging
import socket
from .jsonrpc import ServerProxy, ServerPool
from .exceptions import Fault, ServerUnavailable
from functools import partial

__all__ = ['Session']


class Session(object):
    '''RPC Session'''

    # TODO: Https connection management using fingerprint and CA_CERTS
    _CA_CERTS = None
    _FINGERPRINTS = {}

    ServerProxy = partial(ServerProxy, fingerprints=_FINGERPRINTS,
        ca_certs=_CA_CERTS)
    ServerPool = partial(ServerPool, fingerprints=_FINGERPRINTS,
        ca_certs=_CA_CERTS)

    def __init__(self):
        self._connection = None
        self._user = None
        self._user_name = ''
        self._host = ''
        self._port = None
        self._database = ''
        self._context = {}
        self._pwd = None

    def login(self, username, password, host, port, database):
        # backward compatibility
        self._pwd = password
        func = lambda parameters: self._login(
            host, port, database, username, parameters)
        parameters = {}
        while True:
            try:
                func(parameters)
            except Fault as exception:
                if exception.faultCode.startswith('403'):
                    # Unauthorized error
                    raise
                if exception.faultCode != 'LoginException':
                    raise
                name, message, type_ = exception.args
                value = getattr(self, 'get_%s' % type_)(message)
                if value is None:
                    raise Exception('QueryCanceled')
                parameters[name] = value
                continue
            else:
                break

    def _login(self, host, port, database, username, parameters,
            language=None):
        connection = ServerProxy(host, port, database)
        logging.getLogger(__name__).info('common.db.login(%s, %s)' %
            (username, 'x' * 10))
        result = connection.common.db.login(username, parameters, language)
        logging.getLogger(__name__).debug(repr(result))
        self._user = result[0]
        self._user_name = username
        session = ':'.join(map(str, [username] + result))
        if self._connection is not None:
            self._connection.close()
        self._connection = ServerPool(host, port, database, session=session)
        self._host = host
        self._port = port
        self._database = database

    def logout(self):
        if self._connection is not None:
            try:
                logging.getLogger(__name__).info('common.db.logout()')
                with self._connection() as conn:
                    conn.common.db.logout()
            except (Fault, socket.error, http.client.CannotSendRequest):
                pass
            self._connection.close()
            self._connection = None
        self._user = None
        self._user_name = ''
        self._host = ''
        self._port = None
        self._database = ''
        self._context = {}

    def execute(self, *args, **kwargs):
        if self._connection is None:
            raise Fault('403', 'Not loged to server')
        try:
            name = '.'.join(args[:3])
            args = args[3:]
            rpc_context = self._context.copy()
            if kwargs.get('context'):
                rpc_context.update(kwargs['context'])
            args += (rpc_context,)
            logging.getLogger(__name__).info('%s%s' % (name, args))
            with self._connection() as conn:
                result = getattr(conn, name)(*args)
        except (http.client.CannotSendRequest, socket.error) as exception:
            raise ServerUnavailable(*exception.args)
        logging.getLogger(__name__).debug(repr(result))
        return result

    def get_password(self, message):
        return self._pwd
